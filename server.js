// server.js
const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const app = express();

app.listen(3000, function () {
  console.log("listen on 3000");
});

app.use(express.static("public"));

app.use(bodyParser.json());

// mongodb+srv://0712263661:qXFrIa1Nwxa4rLgV@cluster0.awbaw.mongodb.net/test?retryWrites=true&w=majority

// Connect to database
const connectionString =
  "mongodb+srv://0712263661:qXFrIa1Nwxa4rLgV@cluster0.awbaw.mongodb.net/test?retryWrites=true&w=majority";
// MongoClient.connect(connectionString, { useUnifiedTopology : true }, (err, client) => {
//     if (err) return console.error(err)
//     console.log('Connected to database')
// })
MongoClient.connect(connectionString, { useUnifiedTopology: true })
  .then((client) => {
    console.log("Connected to database");
    const db = client.db("star-wars-quotes");

    // ejs template engine
    app.set("view engine", "ejs");

    app.use(bodyParser.urlencoded({ extended: true }));

    const quotesCollection = db.collection("quotes");

    app.get("/", (req, res) => {
      db.collection("quotes")
        .find()
        .toArray()
        .then((results) => {
          res.render("index.ejs", { quotes: results });
        })
        .catch((error) => console.error(error));
      // res.render("index.ejs", {});
    });

    app.post("/quotes", (req, res) => {
      // console.log(req.body)
      quotesCollection
        .insertOne(req.body)
        .then((result) => {
          // console.log(result)
          res.redirect("/");
        })
        .catch((error) => console.error(error));
    });

    // update request
    app.put("/quotes", (req, res) => {
      quotesCollection
        .findOneAndUpdate(
          { name: "Yoda" },
          {
            $set: {
              name: req.body.name,
              quote: req.body.quote,
            },
          },
          { upsert: true }
        )
        .then((result) => {
          res.json("Success");
        })
        .catch((error) => console.error(error));
    });

    // delete request
    app.delete("/quotes", (req, res) => {
      quotesCollection.deleteOne({ name: req.body.name })
        .then((result) => {
          if (result.deleteCount === 0) {
            return res.json("No quote to delete");
          }
          res.json(`Deleted Darth vadar's quote`);
        })
        .catch((error) => console.error(error));
    });

    // console.log('May Node be with you')
  })
  .catch((error) => console.error(error));


